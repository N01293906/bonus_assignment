﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PrimeNo.aspx.cs" Inherits="BonusAssignment.PrimeNo" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="fullscreen" runat="server">
    <h1>Check Prime no</h1>
        <div>
        <!--<asp:Label runat="server" AssociatedControlID="number"></asp:Label>-->
        <asp:TextBox runat="server" ID="number" placeholder="Number to check"></asp:TextBox>
        <asp:RequiredFieldValidator ID="validatorNumber" ForeColor="Red" runat="server" ErrorMessage="Must Enter a value" ControlToValidate="number"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator runat="server" ControlToValidate="number" ValidationExpression="^[0-9]*$" ErrorMessage="Input must be a number" ForeColor="Red"></asp:RegularExpressionValidator>
        </div>
        <div>
        <asp:Button runat="server" Text="Check" OnClick="CheckPrime" ID="checkButton"/>
        </div>
        <div>
            <h2>Result:- </h2>
            <h3 id="resultPrint" runat="server"></h3>
        </div>
</asp:Content>
