﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Palindrome.aspx.cs" Inherits="BonusAssignment.Palindrome" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="fullscreen" runat="server">
      <h1>Check Palindrome</h1>
        <div>
        <!--<asp:Label runat="server" AssociatedControlID="palindrome"></asp:Label>-->
        <asp:TextBox runat="server" ID="palindrome" placeholder="Enter String"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="palindrome" ErrorMessage="Must enter a string" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ControlToValidate="palindrome" ValidationExpression="^[a-zA-Z\s
]+$" ErrorMessage="String must contain characters only" ForeColor="Red"></asp:RegularExpressionValidator>
            <!--REGULAR EXPRESSION VALIDATOR WHICH WILL USER TO ENTER STRING WITH ALPHABETES AND SPACE-->
            <!--https://forums.asp.net/t/1982812.aspx?Regular+expression+to+allow+only+characters+with+space-->
        </div>
        <div>
        <asp:Button runat="server" Text="Check" OnClick="CheckPalindrome" ID="checkButton"/>
        </div>
        <div>
            <h2>Result:- </h2>
            <h3 id="resultPrint" runat="server"></h3>
        </div>
</asp:Content>
