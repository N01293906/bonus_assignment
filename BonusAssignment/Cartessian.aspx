﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cartessian.aspx.cs" Inherits="BonusAssignment.Cartessian" %>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="fullscreen" runat="server">
   
        <h1>Check Quadrant</h1>
        <div>
        <!--<asp:Label runat="server" AssociatedControlID="xValue"></asp:Label>-->
        <asp:TextBox runat="server" ID="xValue" placeholder="X axis value"></asp:TextBox>
        <asp:RequiredFieldValidator ID="validatorXValue" ForeColor="Red" runat="server" ErrorMessage="Must Enter a value" ControlToValidate="xValue"></asp:RequiredFieldValidator>
        </div>
        <div>
       <!-- <asp:Label runat="server" AssociatedControlID="yValue"></asp:Label>-->
        <asp:TextBox runat="server" ID="yValue" placeholder="Y axis Value"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="yValue" ForeColor="Red" ErrorMessage="Must Enter a Value" ID="validatorYValue"></asp:RequiredFieldValidator>
        </div>
        <div>
        <asp:Button runat="server" Text="Check" OnClick="CheckQuadrant" ID="checkButton"/>
        </div>
        <div>
            <h2>Result:- </h2>
            <h3 id="resultPrint" runat="server"></h3>
        </div>
  
</asp:Content>
