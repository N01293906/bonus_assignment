﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class Cartessian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CheckQuadrant(object sender, EventArgs e)
        {
            int x = Int32.Parse(xValue.Text);
            int y = Int32.Parse(yValue.Text);
            if (x == 0 || y == 0)
            { 
                resultPrint.InnerText = "Value of X or Y can not be zero";
            }
            else
            {
                if (x > 0)
                {
                    if (y > 0)//LOGIC FOR 1st QUADRANT
                    {
                        string msg = "The point with coordinate ( ";
                        msg += xValue.Text; ;
                        msg += ",";
                        msg+=yValue.Text;
                        msg+=" ) lies in 1st Quadrant";
                        resultPrint.InnerText = msg;
                    }
                    else if (y < 0)//LOGIC FOR 4th QUADRANT
                    {
                        string msg = "The point with coordinate ( ";
                        msg += xValue.Text; ;
                        msg += ",";
                        msg+=yValue.Text;
                        msg += " ) lies in 4th Quadrant";
                        resultPrint.InnerText = msg;
                        //resultPrint.InnerText = "The point with coordinate ( " + xValue.Text + "," + yValue + ") lies in 4th Quadrant";
                    }
                }
                if (x < 0)
                {
                    if (y > 0)//LOGIC FOR 2nd QUADRANT
                    {
                        string msg = "The point with coordinate ( ";
                        msg += xValue.Text; ;
                        msg += ",";
                        msg += yValue.Text;
                        msg += " ) lies in 2nd Quadrant";
                        resultPrint.InnerText = msg;
                        //string msg1 = "The point with coordinate ( " + xValue.Text + "," + yValue + ") lies in 2nd Quadrant";
                    }
                    else if (y < 0)//LOGIC FOR 3rd QUADRANT
                    {
                        string msg = "The point with coordinate ( ";
                        msg += xValue.Text; ;
                        msg += ",";
                        msg += yValue.Text;
                        msg += " ) lies in 3rd Quadrant";
                        resultPrint.InnerText = msg;
                        //resultPrint.InnerText = "The point with coordinate ( " + xValue.Text + "," + yValue + ") lies in 3rd Quadrant";
                    }
                }
            }
        }
    }
}