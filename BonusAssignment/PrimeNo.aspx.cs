﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class PrimeNo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CheckPrime(object sender, EventArgs e)
        {
            int n =Int32.Parse(number.Text);
            bool flag=false;
            for (int i = 2; i <= n / 2; ++i)
            {
                // CONDITION FOR NON PRIME NO
                if (n % i == 0)
                {
                    flag = true;
                    break;
                }
            }

            if (n == 1)
            {
                resultPrint.InnerText = "1 is neither a prime nor a composite number.";
            }
            else if (n > 0)
            {
                if (flag == false)//LOGIC TO CHECK PRIME NO
                {
                    string msg = number.Text;
                    msg += " is a prime number.";
                    resultPrint.InnerText = msg;
                }
                else
                {
                    string msg = number.Text;
                    msg += " is not a prime number.";
                    resultPrint.InnerText = msg;
                }
            }
            else
                resultPrint.InnerText = "Number should be more than 0";
        }
    }
}