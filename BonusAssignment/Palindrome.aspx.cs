﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class Palindrome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void CheckPalindrome(object sender, EventArgs e)
        {
            string revs="";
            string palindrome1 = palindrome.Text.ToLower();
            palindrome1= palindrome1.Replace(" ", string.Empty);
            for (int i = palindrome1.Length-1;i >= 0; i--) //String Reverse
            {
                revs += palindrome1[i];
            }
            if (revs == palindrome1) // Checking whether string is palindrome or not
            {
                string msg=palindrome.Text;
                msg += " is palindrome";
                resultPrint.InnerText = msg;
            }
            else
            {
                string msg = palindrome.Text;
                msg += " is not a palindrome";
                resultPrint.InnerText = msg;
            }
        }
    }
}